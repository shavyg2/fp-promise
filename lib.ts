

/**
 * Checks if an item is a promise.
 * @param item Test item 
 */
export function isPromise(item) {
    return item && typeof item === 'object' && 'then' in item;
}

export function Combine(...funcs: any[]) {
    
    if (funcs.length < 2) {
        return funcs[0]
    }
    return function (...args: any[]) {



        let results = funcs.reduce((results: any[], _function) => {
            results.push(_function(...args))
            return results;
        }, [] as any[]) as any[];

        let has_promise = results.map(x => isPromise(x)).reduce((x, y) => x || y)
        if (has_promise) {
            results = results.map(x => Promise.resolve(x))
            return Promise.all(results);
        } else {
            return results;
        }

    }
}

export function CombineAsync(...funcs) {
    return async function (...args: any[]) {
        return Combine(...funcs)(...args);
    }
}

export function Composable(_function) {
    return function (...args) {
        if (isPromise(args[0])) {
            return args[0].then(_function)
        } else {
            return _function(...args);
        }
    }
}



export function ComposeFunction(param1, param2) {
    return function (...args) {
        let result = param1(...args);
        return Composable(param2)(result);
    }
}



export function Compose(...args) {
    if (args.length < 2) {
        return args[0]
    }

    return function (...params) {

        let func = args.reduce((prev, next) => {
            return ComposeFunction(prev, next)
        })
        return func(...params);
    }
}


export function ComposeAsync(...args) {
    return async function (...params) {
        return Compose(...args)(...params);
    }
}

function PassAny(args: boolean[]) {
    if(args.length<2){
        return args[0];
    }
    return args.reduce((prev, next) => {
        return prev || next;
    })
}


export function Solve<T>(func: (...args: any[]) => T) {
    return function (...args): (T | Promise<T>) {
        if (PassAny(args.map(isPromise))) {
            //there is a promise here
            let promises = args.map(x => Promise.resolve(x))
            return Promise.all(promises).then((args) => {
                return func(...args)
            })
        } else {
            return func(...args);
        }
    }
}



export function Converge(...funcs) {

    if (funcs.length < 2) {
        return funcs[0]
    }

    return function (...args) {


        let results = [];

        let result = funcs.reduce((prev, next) => {
            if (isPromise(prev)) {
                return prev.then((x) => {
                    return next(...args);
                }).then(x => {
                    results.push(x);
                    return x;
                })
            } else {

                let result = next(...args);
                if (isPromise(result)) {
                    return result.then(x => {
                        results.push(x);
                        return x;
                    });
                } else {
                    results.push(result);
                }
            }
        }, null);


        return isPromise(result) ? result.then(x => results) : results;

    }
}

export function ConvergeAsync(...funcs) {
    return async function (...args) {
        return Converge(...funcs)(...args);
    }
}


export function Spread(func) {
    return function (args: any[]) {
        return func(...args);
    }
}


export function PromiseCB<K=any>(func): Promise<K> {
    return new Promise<K>((resolve, reject) => {
        func((err: Error, result: K) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
    })
}