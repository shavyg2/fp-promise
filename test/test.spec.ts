var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
import * as fs from "fs";

chai.use(chaiAsPromised);
import { isPromise, ComposeFunction, Compose, Solve, Converge, ComposeAsync, Combine, CombineAsync, PromiseCB } from '../lib';

import { expect } from "chai";


describe("Lib", () => {


    describe("IsPromise", () => {

        it("should check if promise", function () {
            let result = isPromise(Promise.resolve('test'))
            expect(result).to.be.true
        })

        it("should check if promise", function () {
            let result = isPromise(() => { })
            expect(result).to.be.false
        })

    })


    describe("ComposeFunction", () => {
        let x2 = x => x * 2
        let x3 = x => x * 3

        let x2Async = async x => x * 2
        let x3Async = async x => x * 3


        it("should double and triple sync", () => {
            const x = ComposeFunction(x2, x3);
            expect(x(1)).to.equal(6);
        })

        it("should double and triple Async", async () => {
            const x = ComposeFunction(x2Async, x3);
            expect(await x(1)).to.equal(6);
        })

        it("should double and triple Async", async () => {
            const x = ComposeFunction(x2, x3Async);
            expect(await x(1)).to.equal(6);
        })

        it("should double and triple Async", async () => {
            const x = ComposeFunction(x2Async, x3Async);
            expect(await x(1)).to.equal(6);
        })
    })



    describe("Compose", () => {

        it("should be able to join multiple sync functions", () => {

            function hello(arg) {
                return `${arg}, hello`;
            }

            function world(arg) {
                return `${arg} world`;
            }

            function period(arg) {
                return `${arg}.`
            }

            let link = Compose(hello, world, period)
            expect(link('test')).to.eql(`test, hello world.`)

        })


        it("should be able to join multiple sync functions", async () => {

            async function hello(arg) {
                return `${arg}, hello`;
            }

            async function world(arg) {
                return `${arg} world`;
            }

            async function period(arg) {
                return `${arg}.`
            }

            let link = Compose(hello, world, period)
            expect(await link('test')).to.eql(`test, hello world.`)

        })


        it("should catch errors", () => {
            expect(function () {
                Compose(function () {
                    throw new Error(`error`)
                })()
            }).to.throw();
        })

        it("should catch async errors", (done) => {
            let error_function = function () {
                return Compose(async function () {
                    throw new Error(`error should be caught`)
                })()
            }

            error_function().then(x => {
                done(new Error(`Error was not caught`))
            }).catch(err => {
                done();
            })
        })


        it("should catch async errors with sync error", (done) => {
            let early_error_function = function () {
                return ComposeAsync(() => {
                    throw new Error('early error')
                }, async function () {
                    throw new Error(`error should be caught`)
                })()
            }

            early_error_function().then(x => {
                done(new Error(`Error was not caught`))
            }).catch(err => {
                done();
            })
        })
    })



    describe("Solve", () => {
        function add(x, y): number {
            return x + y;
        }


        it("should be able to operate on sync params", () => {

            expect(add(1, 2)).to.eq(3)
        })


        it("should be able to operate on sync params", () => {

            expect(Solve(add)(1, 2)).to.eq(3)
        })

        it("should be able to operate on Async params", async () => {

            expect(await Solve(add)(1, Promise.resolve(2))).to.eq(3)
        })
    })


    describe("Converge", () => {

        it("should be able to operate on sync params", () => {

            function double(x) {

                return x * 2;
            }

            function triple(x) {
                return x * 3;
            }
            let double_triple = Converge(double, triple);


            expect(double_triple(1)).to.eqls([2, 3])
        })


        it("should be able to operate on sync params", async () => {

            async function double(x) {

                return x * 2;
            }

            function triple(x) {
                return x * 3;
            }


            function x5(x) {
                return x * 5;
            }
            let double_triple = Converge(double, triple, x5);

            expect(await double_triple(1)).to.eqls([2, 3, 5])
        })

        it("should be able to operate on sync params", async () => {

            function double(x) {

                return x * 2;
            }

            async function triple(x) {
                return x * 3;
            }


            function x5(x) {
                return x * 5;
            }


            let double_triple = Converge(double, triple, x5);

            expect(await double_triple(1)).to.eqls([2, 3, 5])
        })

        it("should be able to operate on sync params", async () => {

            function double(x) {

                return x * 2;
            }

            function triple(x) {
                return x * 3;
            }


            async function x5(x) {
                return x * 5;
            }


            let double_triple = Converge(double, triple, x5);

            expect(await double_triple(1)).to.eqls([2, 3, 5])
        })

    })



    describe("Combine", () => {

        it("should be able to combine sync functions", () => {
            function one() {
                return 1;
            }


            function two() {
                return 2
            }


            let three = Combine(one, two)
            let result = three();


            expect(result).to.eqls([1, 2]);
        })


        it("should be able to combine async functions", (done) => {
            async function one() {
                return 1;
            }


            function two() {
                return 2
            }


            let three = Combine(one, two)
            let result = three() as Promise<any[]>;


            result.then(results => {
                done()
            })
        })
    })


    describe("CombineAsync", () => {

        it("should be able to combine sync functions", (done) => {
            function one() {
                return 1;
            }

            function two() {
                return 2
            }

            let three = CombineAsync(one, two)
            let result = three() as Promise<any[]>;

            result.then(results => {
                expect(results).to.eqls([1, 2]);
                done();
            })
        })
    })


    describe("PromiseCB", () => {

        function _Error(callback) {
            fs.stat(__dirname+"/../fake-package.json",callback);
        }

        function Success(callback) {
            fs.stat(__dirname+"/../package.json",callback);
        }


        it("should to able to resolve callback functions", (done) => {
            PromiseCB(cb => {
                Success(cb);
            }).then((good) => {
                expect(good.isFile()).to.be.true
                done();
            }).catch(done);
        })

        it("should to able to catch async errors", (done) => {
            let result = PromiseCB(cb => {
                _Error(cb);
            }).then((good) => {
                expect(good).to.be.true;
                done(new Error(`Error was not thrown`));
            }).catch(err=>{
                done();
            })

        })


    })
})